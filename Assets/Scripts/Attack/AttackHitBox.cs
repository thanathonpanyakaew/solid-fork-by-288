using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.TryGetComponent<IDamageable>(out var _damageable))
        {
            _damageable.ApplyDamage(gameObject, 1);
        }
    }
}
