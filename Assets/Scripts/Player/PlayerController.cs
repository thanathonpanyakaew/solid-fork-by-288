using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    [SerializeField] private Transform body;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 7f;
    [SerializeField] private LayerMask groundLayer;

    private Vector2 moveInput;
    private bool isGrounded;
    [SerializeField] private Transform spawnPoint;

    public Player player;

    public void OnMove(InputAction.CallbackContext _context)
    {
        moveInput = _context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext _context)
    {
        if (!isGrounded) return;

        player.rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        isGrounded = false;
    }

    private readonly float checkGroundRayLenght = 0.6f;

    private void FixedUpdate()
    {
        //UpdateMovement
        player.rb.velocity = new Vector2(moveInput.x * moveSpeed, player.rb.velocity.y);

        // Flip the player sprite when changing direction
        if (moveInput.x != 0)
        {
            body.localScale = new Vector3(Mathf.Sign(moveInput.x), 1f, 1f);
            float _rotate = Mathf.Sign(moveInput.x) > 0 ? 0 : 180f;
            player.firePoint.rotation = Quaternion.Euler(0, 0, _rotate);
        }

        //CheckGround
        RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, checkGroundRayLenght, groundLayer);

        isGrounded = _hit.collider != null;
    }

    private void UpdateMovement()
    {

    }

    private void CheckGround()
    {

    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, Vector3.down * checkGroundRayLenght, Color.green);
    }


}
